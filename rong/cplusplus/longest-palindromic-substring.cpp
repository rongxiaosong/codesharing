class Solution {
public:
    string longestPalindrome(string s) {
        /* https://oj.leetcode.com/problems/longest-palindromic-substring/
        */
        
        if (s.empty()) return "";
        
        const int n = s.size();
        int start = 0, maxLen = 1;
        
        for (int i = 1; i < s.size(); i++) {
            int len = 1;
            // s[i] as the center
            for (int j = i-1, k = i+1; j >= 0 && k < s.size() && s[j] == s[k]; j--, k++) {
                len += 2;
            }
            if (len > maxLen) {
                maxLen = len;
                start = i - (len-1)/2;
            }
            
            if (s[i] != s[i-1]) continue;
            // s[i-1..i] as the center
            len = 2;
            for (int j = i-2, k = i+1; j >= 0 && k < s.size() && s[j] == s[k]; j--, k++) {
                len += 2;
            }
            if (len > maxLen) {
                maxLen = len;
                start = i - 1 - (len-2)/2;
            }
        }
        
        return s.substr(start, maxLen);
    }
};